#! /usr/bin/env python3

from setuptools import setup

import fdroid_website_search.settings


setup(
    name='fdroid-website-search',
    version=fdroid_website_search.settings.VERSION,
    description='Simple Django-App for searching through F-Droid repositories.',
    author='Michael Poehn',
    author_email='michael.poehn@fsfe.org',
    url='https://gitlab.com/fdroid/fdroid-website-search',
    license='AGPL-3.0',
)
